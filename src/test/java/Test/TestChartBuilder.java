package Test;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

import builder.ChartBuilder;
import chart.ChartSerie;
import chart.ChartSetting;
import chart.ChartType;
import chart.Point;
import chart.SerieType;

public class TestChartBuilder {

private ChartBuilder chBuilder;
	
	public void before(){
		chBuilder = new ChartBuilder();
	}
	
	public void testSerieAdded() {
		
		String title = "test";
		ChartSerie chartSerie = new ChartSerie( );
		chartSerie.setLabel(title);
		
		ChartSetting chartSetting = chBuilder.addSerie(chartSerie).build();
		
		assertThat(chartSetting.getSeries().get(0).getLabel()).isEqualTo(title);
	}
	
	public void testWithSeries(){
		
		List<ChartSerie> list = new ArrayList<ChartSerie>();
			list.add(new ChartSerie());
			list.add(new ChartSerie());
			list.add(new ChartSerie());
		
		ChartSetting chartSetting = chBuilder.withSeries(list).build();
		
		assertThat(chartSetting.getSeries()).hasSize(3);
	}
	
	public void testWithTitle(){
		
		String title = "test";
		
		ChartSetting chartSetting = chBuilder.withTitle(title).build();
		
		assertThat(chartSetting.getTitle()).isEqualTo(title);
	}
	
	@Test
	public void testWithLegend(){
		
		ChartSetting chartSetting = chBuilder.withLegend().build();
		
		assertThat(chartSetting.isHaveLegend()).isTrue();
	}

	public void testWithType(){
		
		ChartType chartType = ChartType.LinePoint;
		
		ChartSetting chartSetting = chBuilder.withType(chartType).build();
		
		assertThat(chartSetting.getCharType()).isEqualTo(chartType);
	}
}
