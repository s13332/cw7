package Test;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

import builder.SerieBuilder;
import chart.ChartSerie;
import chart.Point;
import chart.SerieType;

public class TestSerieBuilder {
	
	private SerieBuilder sBuilder;
	
	public void before(){
		sBuilder = new SerieBuilder();
	}

	public void testPointsAdded() {
		
		Point point = new Point(1,2);
		
		ChartSerie chartSerie = sBuilder.addPoints(point).build();
		
		assertThat(chartSerie.getPoints()).containsExactly(point);

	}
	
	public void testLabelAdded(){
		
		String label = "check";
		
		ChartSerie chartSerie = sBuilder.addLabel(label).build();
		
		assertThat(chartSerie.getLabel()).contains(label);
	}
	
	
	public void testListPointAdded(){
		
		List<Point> point = new ArrayList<Point>();
			point.add( new Point( 1,1 ) );
			point.add( new Point( 1,2 ) );
			point.add( new Point( 1,3 ) );
			point.add( new Point( 1,4 ) );
		
		ChartSerie chartSerie = sBuilder.withPoints(point).build();
			
		assertThat( chartSerie.getPoints()).hasSize( 4 );
	}

	public void testTypeAddes(){
		
		SerieType type = SerieType.Line;
		
		ChartSerie chartSerie = sBuilder.setType(type).build();
		
		assertThat(chartSerie.getSerieType()).isEqualTo(type);
	}

}
